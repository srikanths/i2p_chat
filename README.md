# Overview #

This application is a combination of the Google Speech Recognition API and Artificial Intelligence Markup Language (AIML) based chatbot. This application is currently integrated with i2p.

### Installation ###

* Google Speech Recognition API
The Google Speech Recognition library for Python can be downloaded and installed from Python Package Index using 'easy_install' or 'pip'. It also requires other Python libraries such as PyAudio, PySpeech. 
* Chatbot
The chatbot uses the XML based Artificial Intelligence Markup Language (AIML) for building the dictionary. It uses various natural language processing and AI based syntax.
An AIML interpreter is required to run the chatbot. This application uses a Python based AIML interpreter called PyAIML. This can also be downloaded and installed from Python Package Index or also from sourceforge.

### Set Up###
The details of the various files in the application is as follows.

* i2p_nlp.py:
The application to run the chatbot.
* std-startup.xml:
The config file which loads the dictionary to the chatbot.
* 'standard':
Contains the dictionary of the chatbot. It is an AIML based dictionary
* testSp.py:
To run only the chatbot without Speech Recogniton and i2p. For testing purpose.