#
# Autogenerated by Thrift Compiler (0.9.0-dev)
#
# DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
#
#  options string: py
#

from thrift.Thrift import TType, TMessageType, TException, TApplicationException
from ttypes import *
from thrift.Thrift import TProcessor
from thrift.transport import TTransport
from thrift.protocol import TBinaryProtocol, TProtocol
try:
  from thrift.protocol import fastbinary
except:
  fastbinary = None


class Iface:
  def faceRecognized(self, sensorID, timestamp, name, position):
    """
    Parameters:
     - sensorID
     - timestamp
     - name
     - position
    """
    pass

  def emotionRecognized(self, sensorID, timestamp, emotion, name):
    """
    Parameters:
     - sensorID
     - timestamp
     - emotion
     - name
    """
    pass


class Client(Iface):
  def __init__(self, iprot, oprot=None):
    self._iprot = self._oprot = iprot
    if oprot is not None:
      self._oprot = oprot
    self._seqid = 0

  def faceRecognized(self, sensorID, timestamp, name, position):
    """
    Parameters:
     - sensorID
     - timestamp
     - name
     - position
    """
    self.send_faceRecognized(sensorID, timestamp, name, position)

  def send_faceRecognized(self, sensorID, timestamp, name, position):
    self._oprot.writeMessageBegin('faceRecognized', TMessageType.CALL, self._seqid)
    args = faceRecognized_args()
    args.sensorID = sensorID
    args.timestamp = timestamp
    args.name = name
    args.position = position
    args.write(self._oprot)
    self._oprot.writeMessageEnd()
    self._oprot.trans.flush()
  def emotionRecognized(self, sensorID, timestamp, emotion, name):
    """
    Parameters:
     - sensorID
     - timestamp
     - emotion
     - name
    """
    self.send_emotionRecognized(sensorID, timestamp, emotion, name)

  def send_emotionRecognized(self, sensorID, timestamp, emotion, name):
    self._oprot.writeMessageBegin('emotionRecognized', TMessageType.CALL, self._seqid)
    args = emotionRecognized_args()
    args.sensorID = sensorID
    args.timestamp = timestamp
    args.emotion = emotion
    args.name = name
    args.write(self._oprot)
    self._oprot.writeMessageEnd()
    self._oprot.trans.flush()

class Processor(Iface, TProcessor):
  def __init__(self, handler):
    self._handler = handler
    self._processMap = {}
    self._processMap["faceRecognized"] = Processor.process_faceRecognized
    self._processMap["emotionRecognized"] = Processor.process_emotionRecognized

  def process(self, iprot, oprot):
    (name, type, seqid) = iprot.readMessageBegin()
    if name not in self._processMap:
      iprot.skip(TType.STRUCT)
      iprot.readMessageEnd()
      x = TApplicationException(TApplicationException.UNKNOWN_METHOD, 'Unknown function %s' % (name))
      oprot.writeMessageBegin(name, TMessageType.EXCEPTION, seqid)
      x.write(oprot)
      oprot.writeMessageEnd()
      oprot.trans.flush()
      return
    else:
      self._processMap[name](self, seqid, iprot, oprot)
    return True

  def process_faceRecognized(self, seqid, iprot, oprot):
    args = faceRecognized_args()
    args.read(iprot)
    iprot.readMessageEnd()
    self._handler.faceRecognized(args.sensorID, args.timestamp, args.name, args.position)
    return

  def process_emotionRecognized(self, seqid, iprot, oprot):
    args = emotionRecognized_args()
    args.read(iprot)
    iprot.readMessageEnd()
    self._handler.emotionRecognized(args.sensorID, args.timestamp, args.emotion, args.name)
    return


# HELPER FUNCTIONS AND STRUCTURES

class faceRecognized_args:
  """
  Attributes:
   - sensorID
   - timestamp
   - name
   - position
  """

  thrift_spec = (
    None, # 0
    (1, TType.STRING, 'sensorID', None, None, ), # 1
    (2, TType.I64, 'timestamp', None, None, ), # 2
    (3, TType.STRING, 'name', None, None, ), # 3
    (4, TType.STRUCT, 'position', (I2P.ttypes.Vec2, I2P.ttypes.Vec2.thrift_spec), None, ), # 4
  )

  def __init__(self, sensorID=None, timestamp=None, name=None, position=None,):
    self.sensorID = sensorID
    self.timestamp = timestamp
    self.name = name
    self.position = position

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.STRING:
          self.sensorID = iprot.readString();
        else:
          iprot.skip(ftype)
      elif fid == 2:
        if ftype == TType.I64:
          self.timestamp = iprot.readI64();
        else:
          iprot.skip(ftype)
      elif fid == 3:
        if ftype == TType.STRING:
          self.name = iprot.readString();
        else:
          iprot.skip(ftype)
      elif fid == 4:
        if ftype == TType.STRUCT:
          self.position = I2P.ttypes.Vec2()
          self.position.read(iprot)
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('faceRecognized_args')
    if self.sensorID is not None:
      oprot.writeFieldBegin('sensorID', TType.STRING, 1)
      oprot.writeString(self.sensorID)
      oprot.writeFieldEnd()
    if self.timestamp is not None:
      oprot.writeFieldBegin('timestamp', TType.I64, 2)
      oprot.writeI64(self.timestamp)
      oprot.writeFieldEnd()
    if self.name is not None:
      oprot.writeFieldBegin('name', TType.STRING, 3)
      oprot.writeString(self.name)
      oprot.writeFieldEnd()
    if self.position is not None:
      oprot.writeFieldBegin('position', TType.STRUCT, 4)
      self.position.write(oprot)
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    return


  def __repr__(self):
    L = ['%s=%r' % (key, value)
      for key, value in self.__dict__.iteritems()]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

  def __ne__(self, other):
    return not (self == other)

class emotionRecognized_args:
  """
  Attributes:
   - sensorID
   - timestamp
   - emotion
   - name
  """

  thrift_spec = (
    None, # 0
    (1, TType.STRING, 'sensorID', None, None, ), # 1
    (2, TType.I64, 'timestamp', None, None, ), # 2
    (3, TType.I32, 'emotion', None, None, ), # 3
    (4, TType.STRING, 'name', None, None, ), # 4
  )

  def __init__(self, sensorID=None, timestamp=None, emotion=None, name=None,):
    self.sensorID = sensorID
    self.timestamp = timestamp
    self.emotion = emotion
    self.name = name

  def read(self, iprot):
    if iprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None and fastbinary is not None:
      fastbinary.decode_binary(self, iprot.trans, (self.__class__, self.thrift_spec))
      return
    iprot.readStructBegin()
    while True:
      (fname, ftype, fid) = iprot.readFieldBegin()
      if ftype == TType.STOP:
        break
      if fid == 1:
        if ftype == TType.STRING:
          self.sensorID = iprot.readString();
        else:
          iprot.skip(ftype)
      elif fid == 2:
        if ftype == TType.I64:
          self.timestamp = iprot.readI64();
        else:
          iprot.skip(ftype)
      elif fid == 3:
        if ftype == TType.I32:
          self.emotion = iprot.readI32();
        else:
          iprot.skip(ftype)
      elif fid == 4:
        if ftype == TType.STRING:
          self.name = iprot.readString();
        else:
          iprot.skip(ftype)
      else:
        iprot.skip(ftype)
      iprot.readFieldEnd()
    iprot.readStructEnd()

  def write(self, oprot):
    if oprot.__class__ == TBinaryProtocol.TBinaryProtocolAccelerated and self.thrift_spec is not None and fastbinary is not None:
      oprot.trans.write(fastbinary.encode_binary(self, (self.__class__, self.thrift_spec)))
      return
    oprot.writeStructBegin('emotionRecognized_args')
    if self.sensorID is not None:
      oprot.writeFieldBegin('sensorID', TType.STRING, 1)
      oprot.writeString(self.sensorID)
      oprot.writeFieldEnd()
    if self.timestamp is not None:
      oprot.writeFieldBegin('timestamp', TType.I64, 2)
      oprot.writeI64(self.timestamp)
      oprot.writeFieldEnd()
    if self.emotion is not None:
      oprot.writeFieldBegin('emotion', TType.I32, 3)
      oprot.writeI32(self.emotion)
      oprot.writeFieldEnd()
    if self.name is not None:
      oprot.writeFieldBegin('name', TType.STRING, 4)
      oprot.writeString(self.name)
      oprot.writeFieldEnd()
    oprot.writeFieldStop()
    oprot.writeStructEnd()

  def validate(self):
    return


  def __repr__(self):
    L = ['%s=%r' % (key, value)
      for key, value in self.__dict__.iteritems()]
    return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

  def __eq__(self, other):
    return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

  def __ne__(self, other):
    return not (self == other)
